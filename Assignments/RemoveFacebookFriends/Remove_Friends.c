#include<stdio.h>
#include<stdlib.h>
# define null '\0'

typedef struct node{
	int popularity;
	struct node *next;
	struct node *prev;
}node;
struct node *head = null;
struct node *p = null;

struct node *insert(struct node *head , int popularity){
	struct node *tmp = malloc(sizeof(node));
	tmp->popularity = popularity;
	tmp->next = null;
	tmp->prev =null;
	if(head == null){
		head = tmp;
		p = head;
	}
	else{
		p->next = tmp;
		tmp->prev = p;
		p=tmp;
	}
	return head;
}
int delete_count =0;
struct node *algorithm_delete(struct node *head,int K){
	if(head){
		struct node *friend = head;
		struct node *last = friend;
		while(friend != null){
			if(friend->next){
				if(friend->popularity < friend->next->popularity){
					delete_count++;
					if(friend->prev == null){
						friend = friend->next;
						head = friend;
						friend->prev = null;
					}else{
						struct node *tmp = friend;
						friend->prev->next = friend->next;
						friend->next->prev = friend->prev;
						friend = friend->prev;
						tmp->next = null;
						tmp->prev = null;
					}
				}else{
					friend = friend->next;
				}
			}else{
				last = friend;
				break;
			}
			if(delete_count == K){
				break;
			}
		}
		int remaining = K-delete_count;
		while(remaining!=0){
			friend = friend->prev;
			friend->next = null;
			remaining--;
		}
	}
	return head;
}
void display(struct node *head){
	struct node *friend = head;
	while(friend!=null){
		printf("%d ",friend->popularity);
		friend = friend->next;
	}
	printf("\n");
}

int main(){

	int T;
	scanf("%d",&T);
	while(T!=0){
		int N,K,i,popularity_score;
		scanf("%d%d",&N,&K);
		for(i=0;i<N;i++){
			scanf("%d",&popularity_score);
			head = insert(head,popularity_score);
		}
		head = algorithm_delete(head,K);

		display(head);
		T--;
		head = null;
		delete_count = 0;
	}

return 0;
}

import unittest

from array_addition import AddAlternateElements


class TestAddAlternateElements(unittest.TestCase):
    def __init__(self, *args):
        super(TestAddAlternateElements, self).__init__(*args)
        self.add_alternate_elements = AddAlternateElements()
        self.add_alternate_elements.array_elements = [[4, 6, 9], [2, 5, 8], [1, 3, 7]]

    def test_check_input_values(self):
        array = [[4, 6, 9], [2, 5, 8], [1, 3, 7]]
        self.assertEqual(self.add_alternate_elements.array_elements[0][0], 4)
        self.assertNotEqual(self.add_alternate_elements.array_elements[0][2], 7)
        self.assertEqual(self.add_alternate_elements.array_elements[1][1], 5)
        self.assertEqual(self.add_alternate_elements.array_elements[2][2], 7)
        self.assertNotEqual(self.add_alternate_elements.array_elements[2][0], 9)
        self.assertEqual(self.add_alternate_elements.array_element, array)

    def test_check_total_values(self):
        self.assertEqual(self.add_alternate_elements.check_total_values, 9)
        self.assertNotEqual(self.add_alternate_elements.check_total_values, 7)
        self.assertNotEqual(self.add_alternate_elements.check_total_values, 6)
        self.assertNotEqual(self.add_alternate_elements.check_total_values, 5)
        self.assertNotEqual(self.add_alternate_elements.check_total_values, 4)
        self.assertNotEqual(self.add_alternate_elements.check_total_values, 3)
        self.assertNotEqual(self.add_alternate_elements.check_total_values, 2)
        self.assertNotEqual(self.add_alternate_elements.check_total_values, 1)
        self.assertNotEqual(self.add_alternate_elements.check_total_values, 0)




    def test_add_even_values_sum(self):
        self.assertEqual(
            self.add_alternate_elements.add_even_values(self.add_alternate_elements.array_elements),
            25)
        self.assertNotEqual(
            self.add_alternate_elements.add_even_values(self.add_alternate_elements.array_elements), 20)

    def test_add_odd_values_sum(self):
        self.assertEqual(
            self.add_alternate_elements.add_odd_values(self.add_alternate_elements.array_elements),
            20)
        self.assertNotEqual(
            self.add_alternate_elements.add_odd_values(self.add_alternate_elements.array_elements), 25)


if _name_ == '__main__':
    unittest.main()
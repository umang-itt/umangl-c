#include <iostream>

using namespace std;
 
struct binarySearchTree
{
    int inputValue;
    binarySearchTree *left;
    binarySearchTree *right;
};
 
class MonkWatchingFight
{

    public: 
        binarySearchTree *createnode(int);
        binarySearchTree *insertNodeToTree(binarySearchTree *, int);
        int getHeight(binarySearchTree *);
};


binarySearchTree* MonkWatchingFight::createnode(int key)
{
    
    node=new binarySearchTree;
    node->inputValue=key;
    node->left=NULL;
    node->right=NULL;
    return node;
}
 
binarySearchTree* MonkWatchingFight::insertNodeToTree(binarySearchTree *root,int key)
{
    if(root==NULL)
    {
        return bst.createnode(key);
    }
    
     if(key<=root->inputValue)
    {
        root->left=bst.insertNodeToTree(root->left,key);
    }
    
    else if(key>=root->inputValue)
    {
        root->right= bst.insertNodeToTree(root->right,key);
    }
    
    return root;
}
 
int MonkWatchingFight::getHeight(binarySearchTree *root)
{
    if(root==NULL)
    {
        return 0;
    }
    
    int left=getHeight(root->left);
    int right=getHeight(root->right);
    return (1+max(left,right));
}
 
int main()
{
    int totalValues;
    cin>>totalValues;
    int key;

    MonkWatchingFight bst;

    int InputValuesArray[10000];
    for(int iterator=0;iterator<totalValues;iterator++)
    {
        cin>>key;
        InputValuesArray[iterator]=key;
    }
    binarySearchTree *root=NULL;
    
    for(int i=0;i<totalValues;i++)
    {
        root= bst.insertNodeToTree(root,InputValuesArray[i]);
    }
    
    int height= bst.getHeight(root);
    cout<<height;
    return 1;
}
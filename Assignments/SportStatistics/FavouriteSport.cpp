#include<iostream>
#include<map>

using namespace std;

map<string,string>personDetails;
map<string,int>sportPopularity;
map<string,int>:: iterator keyIterator, position;



void getPersonDetails(){
    string personName, sportName;
    int sportLikes=1;
    cin>>personName >> sportName;
    if(personDetails.count(personName) == 0){
        
        personDetails.insert(pair<string,string>(personName,sportName));

        if(sportPopularity.count(sportName)==0){

            sportPopularity.insert(pair<string,int>(sportName,sportLikes));
        }
        else{
            
            (sportPopularity.find(sportName)->second)++;
        }
    }
}

string mostPopularSport(){
    int maxLike=-1;
    string favouriteSport;

    for(keyIterator=sportPopularity.begin();  keyIterator!=sportPopularity.end(); keyIterator++){
        if(keyIterator->second > maxLike) 
        {
         maxLike = keyIterator->second;
         favouriteSport = keyIterator->first;
        }        
    }
        return favouriteSport;  
}

int footballLikes(){
    position = sportPopularity.find("football");
    if (position == sportPopularity.end()) {
        return 0;
    } 
    else{
        return position->second; 
    }
}

void main(){
        
    int totalPersons, personCounter, likesForFootball;
    string popularSport;
    cin>> totalPersons;

    for(personCounter=0;personCounter<=totalPersons;personCounter++){
        getPersonDetails();
    }   
    popularSport= mostPopularSport();
    likesForFootball= footballLikes(); 
    cout<<popularSport << endl;
    cout<<likesForFootball;
}
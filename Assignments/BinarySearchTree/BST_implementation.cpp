#include <iostream>

using namespace std;
 
struct binarySearchTree
{
    int inputValue;
    binarySearchTree *left;
    binarySearchTree *right;
};
 
binarySearchTree* createnode(int key)
{
    
    node=new binarySearchTree;
    node->inputValue=key;
    node->left=NULL;
    node->right=NULL;
    return node;
}
 
binarySearchTree* insertNodeToTree(binarySearchTree *root,int key)
{
    if(root==NULL)
    {
        return createnode(key);
    }
    
     if(key<=root->inputValue)
    {
        root->left=insertNodeToTree(root->left,key);
    }
    
    else if(key>=root->inputValue)
    {
        root->right=insertNodeToTree(root->right,key);
    }
    
    return root;
}
 
int getHeight(binarySearchTree *root)
{
    if(root==NULL)
    {
        return 0;
    }
    
    int left=getHeight(root->left);
    int right=getHeight(root->right);
    return (1+max(left,right));
}
 
int main()
{
    int totalValues;
    cin>>totalValues;
    int key;
    int InputValuesArray[10000];
    for(int iterator=0;iterator<totalValues;iterator++)
    {
        cin>>key;
        InputValuesArray[iterator]=key;
    }
    binarySearchTree *root=NULL;
    
    for(int i=0;i<totalValues;i++)
    {
        root=insertNodeToTree(root,InputValuesArray[i]);
    }
    
    int height=getHeight(root);
    cout<<height;
    return 1;
}
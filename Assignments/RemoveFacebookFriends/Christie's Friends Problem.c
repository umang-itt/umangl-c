#include<stdio.h>
# define null '\0'


struct node
{
  int friendPopularity;
  struct node *next; 
  
}node;

struct node *headNode=null ,*currentNode=null;
struct node *temp = null;
struct node *insertNode(struct node *headNode , int friendPopularity){
	struct node *temporaryNode = malloc(sizeof(node));
	temporaryNode->friendPopularity = friendPopularity;
	temporaryNode->next = null;

	if(headNode == null){
		headNode = temporaryNode;
		currentNode = headNode;
	}
	else{
		currentNode->next = temporaryNode;
	
		currentNode=temporaryNode;
	}
	return headNode;
}

int deletedFriendCount= 0;
struct node *deleteFriend(struct node *headNode,int friendsToDelete){
	if(headNode){
		struct node *friend = headNode;
		struct node *lastFriend = friend;
	
		while(friend != null){
			if(friend->next){
				if(friend->friendPopularity < friend->next->friendPopularity){
					deletedFriendCount++;
					if(headNode == friend){
						friend = friend->next;
						headNode = friend;
					
					}else{
					
				    	temp->next = friend->next;
				    	friend = friend->next;
					}
				}else{
					temp= friend;
					friend = friend->next;
					
				}
			}else{
				lastFriend = friend;
				break;
			}
			if(deletedFriendCount == friendsToDelete){
				break;
			}
		}
		int remaining = friendsToDelete-deletedFriendCount;
		while(remaining!=0){
			temp->next=null;
			friend = temp;
			
			remaining--;
		}
	}
	return headNode;
}

void display(struct node *headNode){
	struct node *friend = headNode;
	while(friend!=null){
		printf("%d ",friend->friendPopularity);
		friend = friend->next;
	}
	printf("\n");
}
void main(){
	int testCase,currentTotalFriends,friendsToDelete,friendsCount,friendPopularity;
	
	scanf("%d", &testCase);
	while(testCase!=0){
		scanf("%d%d",&currentTotalFriends,&friendsToDelete);
		for(friendsCount=0;friendsCount<currentTotalFriends;friendsCount++){
			
			scanf("%d",&friendPopularity);
			headNode= insertNode(headNode,friendPopularity);	
		}

		headNode = deleteFriend(headNode,friendsToDelete);
		display(headNode);
		testCase--;
		headNode = null;
		deletedFriendCount = 0;		

	}
}
